# Vagrant - LAMP stack

1. Install [Virtual Box](https://www.virtualbox.org/wiki/Downloads) and [Vagrant](https://www.vagrantup.com/downloads.html)

2. Provision the Vagrant box by running `vagrant up`

3. Log into the Vagrant box
  `vagrant ssh`
  `cd /vagrant`
  `npm install`

5. Assign private IP address in `Vagrantfile`

4. Got to private IP in your browser