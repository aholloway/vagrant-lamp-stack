#!/usr/bin/env bash
 
# Update the package list
sudo apt-get update
sudo apt-get upgrade -y
 
# Install PPA
sudo apt-add-repository ppa:chris-lea/node.js
sudo apt-add-repository ppa:ondrej/php5 -y
 
sudo apt-get update
 
# Install base packages (Git, Vim, terminal multiplexer and repository manager)
sudo apt-get install -y unzip git-core ack-grep vim tmux curl wget build-essential python-software-properties

# Set timezone
echo "Europe/London" | sudo tee /etc/timezone && dpkg-reconfigure --frontend noninteractive tzdata
 
# Install PHP and web server related modules
sudo apt-get install -y php5 apache2 libapache2-mod-php5 php5-curl php5-gd php5-mcrypt php5-mysql php5-sqlite php5-xdebug php5-imagick
 
## Enable mode rewrite
sudo a2enmod rewrite
 
## Change a few PHP settings
sudo sed -i "s/error_reporting = .*/error_reporting = E_ALL & ~E_DEPRECATED/" /etc/php5/apache2/php.ini
sudo sed -i "s/display_errors = .*/display_errors = On/" /etc/php5/apache2/php.ini
sudo sed -i "s/memory_limit = .*/memory_limit = 512M/" /etc/php5/apache2/php.ini
sudo sed -i "s/;date.timezone.*/date.timezone = \"Europe/Zurich\"/" /etc/php5/apache2/php.ini
sudo sed -i "s/short_open_tag.*/short_open_tag = On/" /etc/php5/apache2/php.ini
 
sudo sed -i "s/error_reporting = .*/error_reporting = E_ALL & ~E_DEPRECATED/" /etc/php5/cli/php.ini
sudo sed -i "s/display_errors = .*/display_errors = On/" /etc/php5/cli/php.ini
sudo sed -i "s/disable_functions = .*/disable_functions = /" /etc/php5/cli/php.ini
sudo sed -i "s/memory_limit = .*/memory_limit = 512M/" /etc/php5/cli/php.ini
sudo sed -i "s/;date.timezone.*/date.timezone = \"Europe/Zurich\"/" /etc/php5/apache2/php.ini
sudo sed -i "s/short_open_tag.*/short_open_tag = On/" /etc/php5/cli/php.ini
 
## Configure XDebug
cat << EOF | sudo tee -a /etc/php5/mods-available/xdebug.ini
xdebug.cli_color = 1
xdebug.idekey = PHPSTORM
xdebug.remote_connect_back=0
xdebug.remote_port=9000
xdebug.remote_enable=1
xdebug.remoet_host=192.168.1.150
xdebug.show_exception_trace = 1
xdebug.profiler_enable_trigger = 1
xdebug.profiler_output_name = cachegrind.out.%H.%t
xdebug.trace_enable_trigger = 1
EOF
 
## restart
sudo service apache2 restart
 
# Install MySQL
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
 
sudo apt-get install -y mysql-server
 
## Allow remote access
sudo sed -i "s/^bind-address.*/\#bind-address/" /etc/mysql/my.cnf
sudo service mysql restart
 
# Install and configure Sendmail
sudo apt-get install -y sendmail
sudo hostname precise64.localdomain
sudo sed -i "s/^127\.0\.1\.1\tprecise64.*/127\.0\.1\.1\tprecise64 precise64.localdomain/" /etc/hosts
 
# Install Composer
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
 
# Install Node.js and Grunt (optional)
# sudo apt-get install -y nodejs npm
# sudo npm install -g grunt